Source: golang-github-yosssi-ace
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-yosssi-gohtml-dev
Standards-Version: 4.1.4
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-yosssi-ace
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-yosssi-ace.git
Homepage: https://github.com/yosssi/ace
XS-Go-Import-Path: github.com/yosssi/ace

Package: golang-github-yosssi-ace-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-yosssi-gohtml-dev
Suggests: ace
Description: HTML template engine for Go (library package)
 Ace is an HTML template engine for Go.  This is inspired by
 Slim (http://slim-lang.com/) and Jade (http://jade-lang.com/).
 This is a refinement of Gold (http://gold.yoss.si/).
 .
 Example:
 .
   = doctype html
   html lang=en
   head
     title Hello Ace
     = css
       h1 { color: blue; }
   body
     h1 {{.Msg}}
     #container.wrapper
       p..
         Ace is an HTML template engine for Go.
         This engine simplifies HTML coding in Go web application development.
     = javascript
       console.log('Welcome to Ace');
 .
 This package provides the Ace library for the Go Programming Language

Package: ace
Architecture: any
Section: text
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: golang-github-yosssi-ace-dev
Conflicts: libace-perl
Built-Using: ${misc:Built-Using}
Description: HTML template engine for Go (command-line tool)
 Ace is an HTML template engine for Go.  This is inspired by
 Slim (http://slim-lang.com/) and Jade (http://jade-lang.com/).
 This is a refinement of Gold (http://gold.yoss.si/).
 .
 Example:
 .
   = doctype html
   html lang=en
   head
     title Hello Ace
     = css
       h1 { color: blue; }
   body
     h1 {{.Msg}}
     #container.wrapper
       p..
         Ace is an HTML template engine for Go.
         This engine simplifies HTML coding in Go web application development.
     = javascript
       console.log('Welcome to Ace');
 .
 This package provides the /usr/bin/ace command-line tool and example files.
